from django.db import models

# Create your models here.


class City(models.Model):
    name = models.CharField(max_length= 30)

    def __str__(self): #shows the actual city name on the addmin dashboard
        return self.name # takes the name inputed and returns the name from city model 

    class Meta: # show the plural of city as cities instead of citys
        verbose_name_plural = 'cities'