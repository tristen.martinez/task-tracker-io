from django.shortcuts import render
import requests
from django.contrib.auth.decorators import login_required

from weather.models import City
from weather.forms import CityForm
# Create your views here.


@login_required
def weather(request):

    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=imperial&appid=268839863dcc5379eb26dd74b1079368'

    cities = City.objects.all() # returns all the cities in the database

    if request.method == 'POST':
        form = CityForm(request.POST)
        form.save()


    form = CityForm()

    weather_data = []

    for city in cities:

        city_weather = requests.get(url.format(city)).json() # this requests the API data and converts JSON to python data type
        
        weather = {
            'city' : city,
            'temperature' : city_weather['main']['temp'],
            'description' : city_weather['weather'][0]['description'],
            'icon' : city_weather['weather'][0]['icon']
        }

        weather_data.append(weather) # add the data for the current city into out list

    context = {"weather_data" : weather_data, 'form' : form}
    return render(request, 'weather/weather.html', context) #returns the weather.html template with the context of weather.

