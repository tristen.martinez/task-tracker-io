from django.urls import path
from base.views import TaskList, TaskDetail, TaskCreate, TaskUpdate, TaskDelete, CustomLoginView, RegisterPage
from django.contrib.auth.views import LogoutView
from base.views import (
    WeightUpdateView, 
    WeightCreateView, 
    SongUrlCreateView,
    SongUpdateView,
)


urlpatterns = [
    path("login/", CustomLoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(next_page='login'), name="logout"),
    path('register/', RegisterPage.as_view(), name="register"),

    path("", TaskList.as_view(), name="tasks"),
    path("task/<int:pk>/", TaskDetail.as_view(), name="task"),
    path("task-create/", TaskCreate.as_view(), name="task-create"),
    path("task-update/<int:pk>/", TaskUpdate.as_view(), name="task-update"),
    path("task-delete/<int:pk>/", TaskDelete.as_view(), name="task-delete"),
    path("weight/update/<int:pk>/", WeightUpdateView.as_view(), name="weight-update"),
    path("weight/create/", WeightCreateView.as_view(), name="weight-create"),
    path("song-url/create/", SongUrlCreateView.as_view(), name="song-url-create"),
    path("song/update/<int:pk>/", SongUpdateView.as_view(), name="song-update"),
    
]