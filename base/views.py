
from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView, FormView
from django.urls import reverse_lazy
from django.views.generic import View

from django.contrib.auth.views import LoginView

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from base.models import Task, WeightTracker, Song
from weather.views import weather

# Create your views here.
class CustomLoginView(LoginView):
    template_name = "base/login.html"
    fields ='__all__'
    redirect_authenticated_user = True

    def get_success_url(self):
        return reverse_lazy('tasks')


class RegisterPage(FormView):
    template_name = "base/register.html"
    form_class = UserCreationForm
    redirect_authenticated_user = True
    success_url = reverse_lazy("tasks")

    def form_valid(self, form):
        user = form.save()
        if user is not None:
            login(self.request, user)
        return super(RegisterPage, self).form_valid(form)
    
    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('tasks')
        return super(RegisterPage, self).get(*args, **kwargs)



class TaskList(LoginRequiredMixin, ListView):
    model = Task
    context_object_name = "tasks"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tasks'] = context['tasks'].filter(user=self.request.user)
        context['count'] = context['tasks'].filter(complete=False).count()
        context['completed_task'] = context['tasks'].filter(complete=True).count()
        # context['WeightTracker'] = WeightTracker.objects.get()
        try:
            # old_url = Song._meta.get_field('song_url')
            song_id = Song.objects.values('id')
            dic_id = song_id[0]
            new_id = dic_id.get('id')
            context['song_id'] = new_id
            
            old_url = Song.objects.values('song_url')
            # print(old_url)
            dic_url = old_url[0]
            # print(dic_url)
            new_url = dic_url.get("song_url")
            # print(new_url)
            context['song'] = ChopSongUrl(new_url)
        except Song.DoesNotExist:
            context['song'] = 'is there nothing there'
        

        try:
            context['WeightTracker'] = WeightTracker.objects.get()
        except WeightTracker.DoesNotExist:
            context['WeightTracker'] = ""

        
        
        # context['weight2goal'] = weight_app(WeightTracker.objects.current_weight, WeightTracker.objects.goal_weight)
        
    

        search_input = self.request.GET.get('search-area') or ''
        if search_input:
            context['tasks'] = context['tasks'].filter(
                title__startswith=search_input)

        context['search_input'] = search_input

        return context
        

class TaskDetail(LoginRequiredMixin, DetailView):
    model = Task
    context_object_name = "task"
    template_name = "base/task.html"


class TaskCreate(LoginRequiredMixin, CreateView):
    model = Task
    fields = ['title', 'description', 'complete']
    success_url = reverse_lazy("tasks")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(TaskCreate, self).form_valid(form)


class TaskUpdate(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ['title', 'description', 'complete']
    success_url = reverse_lazy("tasks")

class TaskDelete(LoginRequiredMixin, DeleteView):
    model = Task
    context_object_name = "task"
    success_url = reverse_lazy("tasks")


def weight_app(current_weight, goal_weight):
    weight_to_goal = current_weight - goal_weight

    return weight_to_goal
    


class WeightCreateView(LoginRequiredMixin, CreateView):
    model = WeightTracker
    fields = ["current_weight", "goal_weight"]
    success_url = reverse_lazy("tasks")
    template_name = "base/w-create.html"

class WeightUpdateView(LoginRequiredMixin, UpdateView):
    model = WeightTracker
    fields = ["current_weight", "goal_weight"]
    success_url = reverse_lazy("tasks")
    template_name = "base/w-update.html"



class SongUrlCreateView(LoginRequiredMixin, CreateView):
    model = Song
    fields = ['song_url']
    success_url = reverse_lazy("tasks")
    template_name = "base/song-url.html"    

class SongUpdateView(LoginRequiredMixin, UpdateView):
    model = Song
    fields = ["song_url"]
    success_url = reverse_lazy("tasks")
    template_name = "base/song-update.html"


def ChopSongUrl(old_url):
    
    string_url = str(old_url)
    code = string_url[31:54]
    # print(code) # this gets the code
    return "https://open.spotify.com/embed/track/" + code + "utm_source=generator"




class FrontendRenderView(View):
    def get(self, request, *args, **kwargs):
        return render(request, "base/error404.html", {})