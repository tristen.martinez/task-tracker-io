from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    title = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)
    complete = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        
        return self.title

    class Meta:
        ordering = ['complete']

class WeightTracker(models.Model):
    current_weight = models.IntegerField(null=True, blank=True)
    goal_weight = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return "current: " + str(self.current_weight) + " goal: " + str(self.goal_weight) 


class Song(models.Model):
    song_url = models.TextField()

    def __str__(self):
        return "song link"