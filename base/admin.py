from django.contrib import admin
from base.models import Task, WeightTracker, Song
# Register your models here.

class TaskAdmin(admin.ModelAdmin):
    pass

admin.site.register(Task, TaskAdmin)


class WeightTrackerAdmin(admin.ModelAdmin):
    pass

admin.site.register(WeightTracker, WeightTrackerAdmin)

class SongAdmin(admin.ModelAdmin):
    pass

admin.site.register(Song, SongAdmin)



